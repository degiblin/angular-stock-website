import { AngularStockSitePage } from './app.po';

describe('angular-stock-site App', function() {
  let page: AngularStockSitePage;

  beforeEach(() => {
    page = new AngularStockSitePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
